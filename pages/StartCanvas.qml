import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtGraphicalEffects 1.0

import "../js/config.js" as CF
import "../components"
import "../models"
import "../pages"

Page {
    property PageCCSL currentItem

    id: root
    visible: true

    header: ToolBarCCSL {
        id: toolBar
        Material.foreground: "white"
        title: root.currentItem ? root.currentItem.title : ""
        toolButtonMenu: root.currentItem ? root.currentItem.toolButtonMenu : true
    }
    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: Home {
            title: qsTr("Start")
        }
        onCurrentItemChanged: {
            root.currentItem = currentItem
        }
    }
    Shortcut {
        id: shortcutGlobal
        sequences: ["Esc", "Back"]
        onActivated: {
            if (stackView.depth === 1) {
                Qt.quit()
            } else {
                stackView.pop()
            }
        }
    }
    Drawer {
        id: drawer
        width: ((parent.width < 600) ? parent.width : 600) * 0.8
        height: parent.height
        dragMargin: Qt.styleHints.startDragDistance * 1.8
        interactive: toolBar.toolButtonMenu
        ListView {
            property int initialIndex: 0
            id: listView
            anchors.fill: parent
            boundsBehavior: Flickable.OvershootBounds
            currentIndex: initialIndex
            clip: true
            headerPositioning: ListView.OverlayHeader
            header: Item {
                height: 100
                width: parent.width
                BackgroundCCSL {
                    anchors.fill: parent
                }
                Row {
                    anchors.fill: parent
                    anchors.margins: 10
                    Image {
                        id: logo
                        height: parent.height
                        fillMode: Image.PreserveAspectFit
                        source: 'qrc:/assets/icons/skull.svg'
                        sourceSize.width: 25
                        sourceSize.height: 30
                        smooth: true
                    }
                    ColorOverlay {
                        anchors.fill: logo
                        source: logo
                        color: "#FFF"
                    }
                    Label {
                        color: 'white'
                        text: qsTr('s<b>qml</b>eton')
                        font.pixelSize: parent.height / 3
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: logo.right
                        anchors.leftMargin: 10
                    }
                }
            }
            Material.foreground: isDarkMode ? Material.color(
                                                  Material.Grey,
                                                  Material.Shade300) : Material.color(
                                                  Material.Grey,
                                                  Material.Shade700)
            model: PagesListModel {}
            delegate: ItemDelegate {
                property bool isSelected: model.title === currentItem.title
                z: parent.z - 1
                Material.foreground: isSelected ? Material.accent : isDarkMode ? Material.color(Material.Grey, Material.Shade300) : Material.color(Material.Grey, Material.Shade700)
                width: parent.width
                text: model.title
                icon.source: model.sourceIcon
                highlighted: isSelected

                onClicked: {
                    if (!isSelected) {
                        stackView.pop(null, StackView.PushTransition)
                        if (model.index !== listView.initialIndex)
                            stackView.push(model.source, {
                                               "title": title
                                           })
                    }
                    drawer.close()
                }
            }
        }
    }

    ToolTip {
        id: toolTip
        x: parent.width / 2 - width / 2
        y: parent.height - height * 2
        timeout: 1500
        delay: 500
        font.pixelSize: 12
        background: Rectangle {
            radius: 10
            color: "black"
            opacity: 0.6
        }
    }
}
