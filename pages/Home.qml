import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.12

import '../components'

PageCCSL {
    PanelCCSL {
        headerText: qsTr('First section')
        width: parent.width
    }

    Label {
        anchors.centerIn: parent
        text: qsTr('Put something here')
    }

}
