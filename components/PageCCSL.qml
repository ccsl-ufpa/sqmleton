import QtQuick 2.15
import QtQuick.Controls 2.15

import '../js/config.js' as CF

Item {
    property bool isPortrait: this.width < this.height
    property bool toolButtonMenu: true
    property string title: CF.title

    anchors.left: parent ? parent.left : undefined
    anchors.leftMargin: drawer.width * drawer.position
}
