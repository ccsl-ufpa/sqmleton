﻿import QtQuick 2.15
import QtQuick.Controls 2.15

import '../js/config.js' as CF

ToolBar {
    property alias title: labelName.text
    property alias toolButtonMenu: buttonLeft.toolButtonMenu

    id: toolBarAPP
    height: 60

    background: Rectangle {
        id: toolBarBackground
        color: CF.backgroundColor
    }

    contentItem: Row {
        anchors.fill: parent
        anchors.margins: 5
        ToolButton {
            property bool toolButtonMenu
            property string iconName: toolButtonMenu ? 'menu' : 'back'
            id: buttonLeft
            icon.source: 'qrc:/assets/icons/' + iconName + '.svg'
            icon.color: 'white'
            height: parent.height

            background: Rectangle {
                opacity: (parent.pressed) ? 0.15 : 0
            }

            onClicked: {
                if (toolButtonMenu) {
                    drawer.open()
                } else {
                    stackView.pop()
                }
            }
        }
        Label {
            id: labelName
            color: CF.titlePageColor
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            width: parent.width - (buttonLeft.width + buttonRigth.width)
            height: parent.height
            font.bold: true
            elide: Text.ElideRight
        }
        ToolButton {
            id: buttonRigth
            height: parent.height
            width: height
            visible: false
        }
    }
}
