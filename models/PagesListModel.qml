import QtQuick 2.15

ListModel {
    id: modelPagesList

    ListElement {
        nameId: 'home'
        active: true
        title: qsTr('Start')
        source: 'qrc:/pages/Home.qml'
        sourceIcon: 'qrc:/assets/icons/home.svg'
    }
    ListElement {
        nameId: 'about'
        active: true
        title: qsTr('About')
        source: 'qrc:/pages/About.qml'
        sourceIcon: 'qrc:/assets/icons/about.svg'
    }
}
